//
//  VideosVC.swift
//  safal
//
//  Created by Bidhee iMac on 9/28/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit
import ObjectMapper

class VideosTableViewCell: UITableViewCell {
    @IBOutlet weak var ivVideoImage: UIImageView!
    @IBOutlet weak var lblVideoTitle: UILabel!
}

class VideosVC: UIViewController, VideosDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var videosTableView: UITableView!
    var videosList: [VideosInfo] = []
    var VCTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.videosTableView.delegate = self
        self.videosTableView.dataSource = self
        
        let videosListAPI = VideosAPI()
        videosListAPI.delegate = self
        videosListAPI.getVideosList(apiToHit: VIDEOS_LIST_API)
        Loader.sharedInstance.showLoader()
        self.title = VCTitle
        
        self.videosTableView.tableFooterView = UIView()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func didReceiveVideosListSuccessfully(resultDict: AnyObject, status: Bool) {
        if (resultDict["status"] as! String == "success") {
            if let json = resultDict as? [String: Any] {
                if let videosResponse:VideosReponse = Mapper<VideosReponse>().map(JSON: json) {
                    self.videosList = (videosResponse.data?.items)!
                }
                self.videosTableView.reloadData()
            }
        }
        Loader.sharedInstance.removeLoader()
    }
    
    func failedToReceiveVideosList(resultDict: AnyObject, status: Bool) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videosList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "videos_cell", for: indexPath) as!
        VideosTableViewCell
        let url = NSURL(string: BASE_URI + self.videosList[indexPath.row].featuredImage!)
        cell.ivVideoImage.af_setImage(withURL: url! as URL)
        cell.lblVideoTitle.text = self.videosList[indexPath.row].title!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let videoId = self.videosList[indexPath.row].videoId
        self.playInYoutube(youtubeId: videoId!)
    }
    
    func playInYoutube(youtubeId: String) {
        if let youtubeURL = URL(string: "youtube://\(youtubeId)"),
            UIApplication.shared.canOpenURL(youtubeURL) {
            // redirect to app
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(youtubeURL)
            }
        } else if let youtubeURL = URL(string: "https://www.youtube.com/watch?v=\(youtubeId)") {
            // redirect through safari
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
                UIApplication.shared.openURL(youtubeURL)
            }
        }
    }
}
