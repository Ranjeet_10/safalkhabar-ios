//
//  VideosAPI.swift
//  safal
//
//  Created by Bidhee iMac on 9/28/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import Foundation

@objc protocol VideosDelegate {
    @objc optional func didReceiveVideosListSuccessfully(resultDict: AnyObject, status: Bool)
    @objc optional func failedToReceiveVideosList(resultDict: AnyObject, status: Bool)
}

class VideosAPI {
    var delegate: VideosDelegate?
    
    func getVideosList(apiToHit: String){
            APIHandler.sharedInstance.performGETRequest(
                (apiToHit as NSString),
                params: [:],
                success: { (response) in
                    self.delegate?.didReceiveVideosListSuccessfully!(resultDict: response, status: true)
            }) { (error) in
                self.delegate?.failedToReceiveVideosList!(resultDict: error, status: false)
            }
    }
}
