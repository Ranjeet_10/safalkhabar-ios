//
//  Videos.swift
//  safal
//
//  Created by Bidhee iMac on 9/28/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import Foundation
import ObjectMapper

class Videos: Mappable {
  var items: [VideosInfo]?
  
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        items  <- map["items"]
    }
    
    
}
