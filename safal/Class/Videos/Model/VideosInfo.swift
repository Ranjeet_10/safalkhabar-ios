//
//  VideosInfo.swift
//  safal
//
//  Created by Bidhee iMac on 9/28/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import Foundation
import ObjectMapper

class VideosInfo: Mappable {
    var id: String?
    var title: String?
    var featuredImage: String?
    var videoId: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id            <- map["id"]
        title         <- map["title"]
        featuredImage <- map["featuredImage"]
        videoId       <- map["videoId"]
    }
    
}
