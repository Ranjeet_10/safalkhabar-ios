//
//  VideosReponse.swift
//  safal
//
//  Created by Bidhee iMac on 9/28/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import Foundation
import ObjectMapper

class VideosReponse: Mappable {
    var status: String?
    var data: Videos?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        data   <- map["data"]
    }
}
