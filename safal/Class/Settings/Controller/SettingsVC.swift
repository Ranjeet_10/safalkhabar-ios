//
//  SettingsVC.swift
//  safal
//
//  Created by Bidhee iMac on 10/1/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit
import OneSignal

class SettingsVC: UIViewController {

    @IBOutlet weak var switchNotification: UISwitch!
    var notificationStatus: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
//        switchNotification.tintColor = self.hexStringToUIColor(hex: "df3237")
        switchNotification.onTintColor = self.hexStringToUIColor(hex: "df3237")
        
        self.title = "सेटिङ"
        
        //get the notificaitons status
        let boolValue = UserDefaults.standard.bool(forKey: checkNotificationsStatus)
        if boolValue {
            switchNotification.isOn = true
        }else{
            switchNotification.isOn = false
        }
        
        
    }
    
    @IBAction func onSwitchChanged(_ sender: Any) {
        if switchNotification.isOn {
            print("Notificaitons subscription is on")
            OneSignal.setSubscription(true)
            UserDefaults.standard.set(true, forKey: checkNotificationsStatus)
        }else {
            print("Notification subscription is Off")
            OneSignal.setSubscription(false)
            UserDefaults.standard.set(false, forKey: checkNotificationsStatus)
        }
        UserDefaults.standard.synchronize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
