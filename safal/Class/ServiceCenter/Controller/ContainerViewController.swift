//
//  ContainerViewController.swift
//  WebViewTest
//
//  Created by Bidhee iMac on 1/22/19.
//  Copyright © 2019 RK. All rights reserved.
//

import UIKit

class NewsViewCell: UITableViewCell {
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newDate: UILabel!
    
}

class ContainerViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var newsTableView: UITableView!
    var ndRelated: [NDInfo] = []
    var ndRelatedMore: [LatestNewsResponseData] = []
    var ndImage: [NDImage] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newsTableView.dataSource = self
        newsTableView.delegate = self
        
        newsTableView.tableFooterView  = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(tableView == newsTableView){
            return 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(tableView == newsTableView) {
            if(section == 0) {
                if(ndRelated.count > 0){
                    return 25.0
                }else{
                    return 0.0
                }
                
            }else {
                return 25.0
            }
        }else{
            return 25.0
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 25.0))
        let lbl = UILabel.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20.0))
        
        lbl.textAlignment = .center
        lbl.center.y = view.center.y
        lbl.textColor = UIColor.white
        lbl.font = UIFont.systemFont(ofSize: 16.0, weight: .medium)
        
        view.backgroundColor = APP_PRIMARY_COLOR
        
        
        if(tableView == newsTableView) {
            if(section == 0) {
                if(ndRelated.count > 0){
                    lbl.text = "सम्बन्धित समाचारहरु"
                    view.addSubview(lbl)
                    return view
                }else{
                    return nil
                }
                
            }else {
                lbl.text = "अन्य समाचार पढ्नुहोस्"
                view.addSubview(lbl)
                return view
            }
        }else{
            lbl.text = "अतिरिक्त फोटोहरू"
            view.addSubview(lbl)
            return view
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == newsTableView) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell") as! NewsViewCell
            
            if(indexPath.section == 0) {
                
                let value = ndRelated[indexPath.row]
                
                
                let imageURL = NSURL(string: BASE_URI + value.featureImage!)
                
                cell.newsImage.af_setImage(withURL: imageURL! as URL)
                
                cell.newsTitle.text = value.title!
                cell.newDate.isHidden = true
                
                return cell
                
            }else if(indexPath.section == 1){
                
                let value = ndRelatedMore[indexPath.row]
                
                let imageURL = NSURL(string: BASE_URI + value.featuredImage!)
                
                cell.newsImage.af_setImage(withURL: imageURL! as URL)
                
                cell.newsTitle.text = value.title!
                cell.newDate.text = value.nepaliDate!
                
                
            }
            
            cell.separatorInset = .zero
            cell.layoutMargins = .zero
            
            return cell
            
        }else {
            
            let value = ndImage[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell") as! NewsViewCell
            
            let imageURL = NSURL(string: BASE_URI + value.image!)
            
            //  cell.ivImage.af_setImage(withURL: imageURL! as URL)
            
            // cell.separatorInset = .zero
            // cell.layoutMargins = .zero
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(tableView == newsTableView) {
            
            var id: Int?
            
            if(indexPath.section == 0) {
                
                id = self.ndRelated[indexPath.row].id
                
            }else if(indexPath.section == 1){
                
                id = self.ndRelatedMore[indexPath.row].id as! Int
            }
            
            let detailsNewsVC = SERVICE_CENTER_STORY_BOARD.instantiateViewController(withIdentifier: "ServiceCenterVC") as! ServiceCenterVC
            
            detailsNewsVC.newsId = id
            self.navigationController?.pushViewController(detailsNewsVC, animated: true)
            
            
            
        }else {
            
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(tableView == newsTableView) {
            
            
            if(indexPath.section == 0) {
                return 95.0
            }else {
                return 95.0
            }
            
        }else{
            return 120.0
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == newsTableView){
            
            if(section == 0){
                return self.ndRelated.count
            }else{
                return self.ndRelatedMore.count
            }
            
        }else {
            
            return self.ndImage.count
            
        }
    }
    
}
