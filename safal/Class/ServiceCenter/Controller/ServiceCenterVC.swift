//
//  ServiceCenterVC.swift
//  agni
//
//  Created by Bidhee iMac on 8/21/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit
import ObjectMapper
import WebKit

class ServiceCenterTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ivShowRoomImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTime: UILabel!
}

class ImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ivImage: UIImageView!
}

class ServiceCenterVC: UIViewController, NDAPIDelegate {
    
    //@IBOutlet weak var serviceCenterTableView: UITableView!
   // @IBOutlet weak var imageTableView: UITableView!
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var ntitle: UILabel!
    
   // var webView: WKWebView!
    @IBOutlet weak var txtView: UITextView!
   // @IBOutlet weak var web: UIView!
    
  //  @IBOutlet weak var imageTbHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    
    var ndRelated: [NDInfo] = []
    var ndRelatedMore: [LatestNewsResponseData] = []
    var ndImage: [NDImage] = []
    var url : String = ""
    
    var VCTitle: String?
    var newsId: Int?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  self.imageTableView.delegate = self
      //  self.imageTableView.dataSource = self
      //  self.imageTableView.tableFooterView = UIView()
        
        
      //  self.serviceCenterTableView.delegate = self
      //  self.serviceCenterTableView.dataSource = self
      //  self.serviceCenterTableView.tableFooterView = UIView()
    
       // webView = WKWebView(frame: .init(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: self.view.frame.width, height: self.web.frame.size.height)))
        
        //webView.navigationDelegate = self
        //self.web.addSubview(webView)
       // webView.scrollView.isScrollEnabled = false
        
                
       // self.title = VCTitle
        
        self.getNavigationBarImage()
      //  print(newsId)
 
        
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        let ndAPI = NDAPI()
        ndAPI.delegate = self
        ndAPI.getND(id: "\(newsId!)")
        Loader.sharedInstance.showLoader()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.webViewHeightConstant.constant = 0.0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
//    override func loadView() {
//        let webConfiguration = WKWebViewConfiguration()
//        webView = WKWebView(frame: .init(origin: self.web.frame.origin, size: self.web.frame.size), configuration: webConfiguration)
//        webView.uiDelegate = self
//        web = webView
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //if(tableView == serviceCenterTableView){
        //    return 2
        //}else{
            return 1
        //}
    }
    
 //   func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if(tableView == serviceCenterTableView) {
//            if(section == 0) {
//                if(ndRelated.count > 0){
//                    return "First"
//                }else{
//                    return nil
//                }
//
//            }else {
//                return "second"
//            }
//        }else{
//            return "Photo"
//
//        }
    
        
 //   }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 25.0))
        let lbl = UILabel.init(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20.0))
        
        lbl.textAlignment = .center
        lbl.center.y = view.center.y
        lbl.textColor = UIColor.white
        lbl.font = UIFont.systemFont(ofSize: 16.0, weight: .medium)
       
        view.backgroundColor = APP_PRIMARY_COLOR
        
        
       // if(tableView == serviceCenterTableView) {
            if(section == 0) {
                if(ndRelated.count > 0){
                    lbl.text = "सम्बन्धित समाचारहरु"
                    view.addSubview(lbl)
                    return view
                }else{
                    return nil
                }
                
            }else {
                lbl.text = "अन्य समाचार पढ्नुहोस्"
                view.addSubview(lbl)
                return view
            }
      //  }else{
        //   lbl.text = "अतिरिक्त फोटोहरू"
        //    view.addSubview(lbl)
        //    return view
      //  }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       // if(tableView == serviceCenterTableView) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "detail_news_cell") as! ServiceCenterTableViewCell
            
            if(indexPath.section == 0) {
                
                let value = ndRelated[indexPath.row]
                
                
                let imageURL = NSURL(string: BASE_URI + value.featureImage!)
                
                cell.ivShowRoomImage.af_setImage(withURL: imageURL! as URL)
                
                cell.lblName.text = value.title!
                cell.lblTime.isHidden = true
                
                return cell
                
            }else if(indexPath.section == 1){
                
                let value = ndRelatedMore[indexPath.row]
                
                let imageURL = NSURL(string: BASE_URI + value.featuredImage!)
                
                cell.ivShowRoomImage.af_setImage(withURL: imageURL! as URL)
                
                cell.lblName.text = value.title!
                cell.lblTime.text = value.nepaliDate!
                
                
            }
            
            cell.separatorInset = .zero
            cell.layoutMargins = .zero
            
            return cell
            
       // }else {
            
         //   let value = ndImage[indexPath.row]
            
          //  let cell = tableView.dequeueReusableCell(withIdentifier: "nd_image") as! ImageTableViewCell
            
          //  let imageURL = NSURL(string: BASE_URI + value.image!)
            
          //  cell.ivImage.af_setImage(withURL: imageURL! as URL)
            
           // cell.separatorInset = .zero
           // cell.layoutMargins = .zero
            
        //    return cell
            
       // }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       // if(tableView == serviceCenterTableView) {
            
            var id: Int?
            
            if(indexPath.section == 0) {
                
                id = self.ndRelated[indexPath.row].id
                
            }else if(indexPath.section == 1){
                
                id = self.ndRelatedMore[indexPath.row].id as! Int
            }
            
            let detailsNewsVC = SERVICE_CENTER_STORY_BOARD.instantiateViewController(withIdentifier: "ServiceCenterVC") as! ServiceCenterVC
            
            detailsNewsVC.newsId = id
            self.navigationController?.pushViewController(detailsNewsVC, animated: true)
            
           
            
      //  }else {
            
           
            
     //   }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
     //   if(tableView == serviceCenterTableView) {
            
            
            if(indexPath.section == 0) {
                return 105.0
            }else {
                return 105.0
            }
            
      //  }else{
          //  return 120.0
            
      //  }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // if(tableView == serviceCenterTableView){
            
            if(section == 0){
                return self.ndRelated.count
            }else{
                return self.ndRelatedMore.count
            }
            
       // }else {
            
        //    return self.ndImage.count
            
       // }
    }
    
    func didReceiveNDSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        print(resultDict)
        if (resultDict["status"] as! String == "success") {
            if let json = resultDict as? [String: Any] {
                if let ndResponse:NDResponse = Mapper<NDResponse>().map(JSON: json) {
                    self.ndImage = ndResponse.data!.additionalImages!
                    self.ndRelated = ndResponse.data!.relatedNews!
                    self.ndRelatedMore = ndResponse.data!.ndMoreRelated!
                    
                 //   self.serviceCenterTableView.reloadData()
                  //  self.imageTableView.reloadData()
                    
                    if(ndResponse.data!.additionalImages!.count > 0){
                   //         self.imageTbHeightConstant.constant = self.imageTableView.contentSize.height
                    }else{
                   //     self.imageTbHeightConstant.constant = 0.0
                    }
                    
                    
                    
                    
                    
                    let imageURL = NSURL(string: BASE_URI + (ndResponse.data?.featuredImage)!)
                    self.bannerImage.af_setImage(withURL: imageURL! as URL)
                    //let contentToLoad = "<html><head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><meta name=\"viewport\" content=\"width=device-width,initial-scale=1\"><style> img{height: 200px !important; width: 100% !important; margin-bottom: 5px;}</style></head><body><p align=\"justify\">"+ndResponse.data!.content!+"</p></body></html>"
                    
                    let contentToLoad = "<html><head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><meta name=\"viewport\" content=\"width=device-width,initial-scale=1\"><style> img{height: 200px !important; width: 100% !important; margin-bottom: 5px;} body {font-size: 16px;}</style></head><body>"+ndResponse.data!.content!+"</body></html>"
                    
                    let load = contentToLoad.htmlToAttributedString
                    self.txtView.attributedText = load

                    //self.webView.loadHTMLString(contentToLoad, baseURL: nil)
                    
                    let js = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust='\(50)%'"

                    //self.webView.evaluateJavaScript(js, completionHandler: nil)
                    
                    if (ndResponse.data!.authorName?.isEmpty)! {
                        self.time.text = ndResponse.data!.nepaliDate
                    }else {
                        self.time.text = "\(ndResponse.data!.nepaliDate!) | \(ndResponse.data!.authorName!)"
                    }
                    self.ntitle.text = ndResponse.data!.title!
                    self.url = ndResponse.data!.url!
                    
                    self.containerViewHeight.constant = CGFloat(95 * (self.ndRelated.count + self.ndRelatedMore.count) + 60)
                    
                    self.performSegue(withIdentifier: "table_segue", sender: self)
                    
                }
            }
        }
        Loader.sharedInstance.removeLoader()
    }
    
    func didFailWithNDError(_ error: NSError, resultStatus: Bool) {
        print(error.localizedDescription)
        Loader.sharedInstance.removeLoader()
    }
    
    
    @IBAction func shareTextButton(_ sender: UIButton) {
        
        let text = BASE_URI + self.url
        
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop]
        
        self.present(activityViewController, animated: true, completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "table_segue" {
            if let destination = segue.destination as? ContainerViewController {
                
                destination.ndImage = self.ndImage
                destination.ndRelated = self.ndRelated
                destination.ndRelatedMore = self.ndRelatedMore
            }
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return false
    }
    
    
}
