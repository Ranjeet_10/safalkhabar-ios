//
//  SlideImageFeatureResponse.swift
//  safal
//
//  Created by Nabin on 10/1/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import Foundation
import ObjectMapper

class SlideImageFeatureResponse: Mappable {
    var status: String?
    var data: [SlideImageResponse]?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        data   <- map["data"]
}
    
}
