//
//  SlideImageResponse.swift
//  safal
//
//  Created by Nabin on 10/1/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit
import ObjectMapper

class SlideImageResponse : Mappable {
    var id: String?
    var title: String?
    var photo: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        title <- map["caption"]
        photo <- map["image"]
    }
    
}
