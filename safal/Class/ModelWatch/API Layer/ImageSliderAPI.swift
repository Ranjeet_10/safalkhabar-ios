//
//  ImageSliderAPI.swift
//  safal
//
//  Created by Nabin on 10/1/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit

@objc protocol ImageSliderAPIDelegate {
    @objc optional func didReceiveImageDataSuccessfully(resultDict:AnyObject,resultStatus:Bool)
    @objc optional func didFailWithImageDataError(_ error: NSError,resultStatus:Bool)
}

class ImageSliderAPI {
    
    var delegate:ImageSliderAPIDelegate?
    
    func getImage( ImageId: Int) {
        
        APIHandler.sharedInstance.performGETRequest(
            (IMAGE_SLIDER_API+"\(ImageId)" as NSString),
            params: [:],
            success: { (response) in
                self.delegate?.didReceiveImageDataSuccessfully!(resultDict: response, resultStatus: true)
        }) { (error) in
            self.delegate?.didFailWithImageDataError!(error, resultStatus: false)
        }
    }
    
}


