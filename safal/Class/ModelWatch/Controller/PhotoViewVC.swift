//
//  PhotoVC.swift
//  agni
//
//  Created by Bidhee iMac on 9/25/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit
import ObjectMapper

class PhotoViewCollectionViewCell : UICollectionViewCell {
    @IBOutlet weak var ivPhoto : UIImageView!
    
}


class PhotoViewVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, ImageSliderAPIDelegate, UICollectionViewDelegateFlowLayout {
    
    
    
    var albumId: Int?
    var isFromData:Bool?
    var photoFeatureInfo: PhotoFeatureInfo?
    var slideImageInfo: [SlideImageResponse] = []
    
    @IBOutlet weak var photoViewCollectionView : UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.photoViewCollectionView.dataSource = self
        self.photoViewCollectionView.delegate = self
        
        if(isFromData == true) {
            let imageSlideApiDelegate = ImageSliderAPI()
            imageSlideApiDelegate.delegate = self
            imageSlideApiDelegate.getImage(ImageId: albumId!)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    
    
    func didReceiveImageDataSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        if (resultDict["status"] as! String == "success") {
            if let json = resultDict as? [String: Any] {
                
                if let imageDataResponse:SlideImageFeatureResponse = Mapper<SlideImageFeatureResponse>().map(JSON: json) {
                    self.slideImageInfo = imageDataResponse.data!
                    self.photoViewCollectionView.reloadData()
                    
                }
            }
            
        }
    }
    func didFailWithImageDataError(_ error: NSError, resultStatus: Bool, response: AnyObject) {
        
        print(error.debugDescription)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isFromData == false {
           return 1
        }
        else{
            return self.slideImageInfo.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photo_view", for: indexPath) as!PhotoViewCollectionViewCell
        
        if isFromData == false {
            let url = NSURL(string: BASE_URI + (photoFeatureInfo?.photo)!)
            cell.ivPhoto.af_setImage(withURL: url! as URL)
        }
        else{
            let url = NSURL(string: BASE_URI + slideImageInfo[indexPath.row].photo!)
            cell.ivPhoto.af_setImage(withURL: url! as URL)
            
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height:275)
    }
}

