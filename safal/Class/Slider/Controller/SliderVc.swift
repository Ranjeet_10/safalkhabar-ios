//
//  SliderVc.swift
//  agni
//
//  Created by Nabin on 8/17/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit
import ObjectMapper

class SliderTableViewCell: UITableViewCell {
    @IBOutlet weak var ivSliderImage:UIImageView?
    @IBOutlet weak var lblSliderName:UILabel?
}

class SliderVc: UIViewController , MenuAPIDelegate{
    
    var sliderImageArray = ["1","2","3","4","5","6"]
    var SliderLabelArray = ["सबै समाचार","मुख्य समाचार","प्रचलित समाचार","सफल टि.भी","मोडल वाच","फोटो फिचर"]
    var secondSection = ["सेटिङ", "हाम्रो बारे","प्राईवेसी पोलिसी"]
    var secondSectionImage = ["7", "8","9"]
    var sliderMenuResponse: [SliderMenuResponseData] = []
    var indexPathToSend: Int = 0
    
    @IBOutlet weak var sliderTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sliderTableView.dataSource = self
        sliderTableView.delegate = self
    }
    
}
extension SliderVc:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if(section == 0) {
            return nil
        }else{
            return "प्रेफेरेंस"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(section == 0) {
            return self.SliderLabelArray.count
        }else{
            return self.secondSection.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var sectionArray: [String] = []
        var sectionImageArray: [String] = []
        if(indexPath.section == 0) {
            sectionArray = SliderLabelArray
            sectionImageArray = self.sliderImageArray
        }else {
            sectionArray = secondSection
            sectionImageArray = self.secondSectionImage
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SliderTableViewCell
        let image = sectionImageArray[indexPath.row]
        
        cell.ivSliderImage?.image = UIImage(named: image)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        
        let label  = sectionArray[indexPath.row]
        cell.lblSliderName?.text = label
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let rootVC = UIApplication.shared.keyWindow?.rootViewController
        let childrenVC = rootVC?.childViewControllers
        _ = childrenVC?.first as? UINavigationController
        
        if(indexPath.section == 0) {
            switch indexPath.row {
            case 0:
                let vc = HOME_STORY_BOARD.instantiateViewController(withIdentifier: "MXScrollingViewController") as! MXScrollingViewController
                (self.slideMenuController()?.mainViewController as! UINavigationController).pushViewController(vc, animated: false)
            case 1:
                let vc = NOTICE_STORY_BOARD.instantiateViewController(withIdentifier: "NoticeVC") as! NoticeVC
                vc.isNotForHome = true
                vc.VCTitle = self.SliderLabelArray[indexPath.row]
                vc.homeApiToHit = indexPath.row
                (self.slideMenuController()?.mainViewController as! UINavigationController).pushViewController(vc, animated: false)
            case 2:
                let vc = NOTICE_STORY_BOARD.instantiateViewController(withIdentifier: "NoticeVC") as! NoticeVC
                vc.isNotForHome = true
                vc.isTrending = true
                vc.VCTitle = self.SliderLabelArray[indexPath.row]
                vc.homeApiToHit = indexPath.row
                (self.slideMenuController()?.mainViewController as! UINavigationController).pushViewController(vc, animated: false)
            case 3:
                
                let vc = VIDEOS_STORY_BOARD.instantiateViewController(withIdentifier: "VideosVC") as! VideosVC
                vc.VCTitle = self.SliderLabelArray[indexPath.row]
                (self.slideMenuController()?.mainViewController as! UINavigationController).pushViewController(vc, animated: false)
            case 4:
                let vc = MODEL_WATCH_STORY_BOARD.instantiateViewController(withIdentifier: "ModelWatchVC") as! ModelWatchVC
                vc.apiToHit = MODEL_WATCH_API
                vc.VCTitle = self.SliderLabelArray[indexPath.row]
                (self.slideMenuController()?.mainViewController as! UINavigationController).pushViewController(vc, animated: false)
            case 5:
                let vc = MODEL_WATCH_STORY_BOARD.instantiateViewController(withIdentifier: "ModelWatchVC") as! ModelWatchVC
                vc.apiToHit = PHOTO_FEATURE_API
                vc.VCTitle = self.SliderLabelArray[indexPath.row]
                (self.slideMenuController()?.mainViewController as! UINavigationController).pushViewController(vc, animated: false)
            default:
                let vc = HOME_STORY_BOARD.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                (self.slideMenuController()?.mainViewController as! UINavigationController).pushViewController(vc, animated: false)
                
            }
        }else {
            switch indexPath.row {
            case 0:
                let vc = SETTINGS_STORY_BOARD.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
                (self.slideMenuController()?.mainViewController as! UINavigationController).pushViewController(vc, animated: false)
            case 1:
                let vc = ABOUT_US_STORY_BOARD.instantiateViewController(withIdentifier: "AboutUsVC") as! AboutUsVC
                vc.VCTitle = self.secondSection[indexPath.row]
                (self.slideMenuController()?.mainViewController as! UINavigationController).pushViewController(vc, animated: false)
            case 2:
                if let url = URL(string: "https://www.safalkhabar.com/content/privacy-policy") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:])
                    } else {
                        // Fallback on earlier versions
                        UIApplication.shared.openURL(url)
                    }
                }
            default:
                let vc = HOME_STORY_BOARD.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                (self.slideMenuController()?.mainViewController as! UINavigationController).pushViewController(vc, animated: false)
                
            }
        }
        self.slideMenuController()?.closeLeft()
        
    }
}
