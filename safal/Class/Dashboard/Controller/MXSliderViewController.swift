//
//  MXSliderViewController.swift
//  agni
//
//  Created by Bidhee iMac on 9/23/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit
import MXParallaxHeader
import ImageSlideshow
import ObjectMapper

class MXSliderViewController: UIViewController, MXParallaxHeaderDelegate, DashboardAPIDelegate {
    
    @IBOutlet weak var slide: ImageSlideshow!
    @IBOutlet weak var lblDescription: UILabel!
    
    var bannerimageArray:[AlamofireSource] = []
    var featurePhotoResponseList: [FeatureResponseInfo] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        parallaxHeader?.delegate = self
        
        let dashboardApiDelegate = DashboardAPI()
        dashboardApiDelegate.delegate = self
        dashboardApiDelegate.getImages()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        //let paddedWidth = lblDescription.intrinsicContentSize.width + 2 * 8

        //lblDescription.widthAnchor.constraint(equalToConstant: paddedWidth).isActive = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func parallaxHeaderDidScroll(_ parallaxHeader: MXParallaxHeader) {
        print("hello")
    }
    
    func didReceiveImagesSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        
        if (resultDict["status"] as! String == "success") {
            if let json = resultDict as? [String: Any] {
                if let featureResponse:FeaturePhotoResponse = Mapper<FeaturePhotoResponse>().map(JSON: json) {
                    self.featurePhotoResponseList = featureResponse.featureResponseData!
                    let bannerInfoList = featureResponse.featureResponseData
                    
                    if(bannerInfoList!.count > 0) {
                        self.lblDescription.text = bannerInfoList!.first!.title
                        self.lblDescription.isHidden = false
                    }
                    for item in bannerInfoList! {
                        self.bannerimageArray.append(AlamofireSource(urlString: BASE_URI+item.photo!)!)
                    }
                    
                    
                    slide.backgroundColor = UIColor.white
                    slide.slideshowInterval = 5.0
                    slide.pageControlPosition = PageControlPosition.insideScrollView
                    slide.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
                    slide.pageControl.pageIndicatorTintColor = UIColor.black
                    slide.contentScaleMode = UIViewContentMode.scaleAspectFill
                    slide.setImageInputs(bannerimageArray as [InputSource])
                    
                    slide.currentPageChanged = {(page: Int) in
                        self.slide.setCurrentPage(page, animated: true)
                        self.lblDescription.text = bannerInfoList![page].title
                    }
                    
                    let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
                    slide.addGestureRecognizer(recognizer)
                    
                }
            }
        }
    }
    
    
    func didFailWithImagesError(_ error: NSError, resultStatus: Bool) {
        print(error.localizedDescription)
    }
    
   
    @IBAction func openSliderMenu(_ sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    
    
    @objc func didTap() {
        let detailsNewsVC = SERVICE_CENTER_STORY_BOARD.instantiateViewController(withIdentifier: "ServiceCenterVC") as! ServiceCenterVC
        
        detailsNewsVC.newsId = (self.featurePhotoResponseList[self.slide.currentPage] as FeatureResponseInfo).id
        (self.slideMenuController()?.mainViewController as! UINavigationController).pushViewController(detailsNewsVC, animated: false)
        
    }

    
}
