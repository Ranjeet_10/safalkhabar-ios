//
//  AboutUsAPI.swift
//  safal
//
//  Created by Bidhee iMac on 9/28/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import Foundation

@objc protocol AboutUsAPIDelegate {
    @objc optional func didReceiveAboutUsSuccessfully(resutlDist: AnyObject, status: Bool)
    @objc optional func failedToReceiveAboutUs(result: AnyObject, status: Bool)
}

class AboutUsAPI {
    var delegate : AboutUsAPIDelegate?
    
    func getAboutUsInfo(){
        APIHandler.sharedInstance.performGETRequest(
            ABOUT_US_API as NSString, params: [:],
            success: {(response) in
                self.delegate?.didReceiveAboutUsSuccessfully!(resutlDist: response, status: true)
        }, failure: {(error) in
            self.delegate?.failedToReceiveAboutUs!(result: error, status: false)
        })
    }

}
