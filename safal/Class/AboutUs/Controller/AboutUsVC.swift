//
//  AboutUsVC.swift
//  safal
//
//  Created by Bidhee iMac on 9/28/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit
import WebKit
import ObjectMapper

class AboutUsVC: UIViewController, AboutUsAPIDelegate, WKUIDelegate {
    
    @IBOutlet weak var wvAboutUs: WKWebView!
    var aboutUsInfo: AboutUsResponse?
    var VCTitle: String?
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let aboutUsAPI = AboutUsAPI()
        aboutUsAPI.delegate = self
        aboutUsAPI.getAboutUsInfo()
        self.title = VCTitle
        Loader.sharedInstance.showLoader()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func didReceiveAboutUsSuccessfully(resutlDist: AnyObject, status: Bool) {
        if (resutlDist["status"] as! String == "success") {
            if let json = resutlDist as? [String: Any] {
                if let aboutUsResponse:AboutUsResponse = Mapper<AboutUsResponse>().map(JSON: json) {
                    self.aboutUsInfo = aboutUsResponse
                    
                    let contentToLoad = "<html><head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><meta name=\"viewport\" content=\"width=device-width,initial-scale=1\"><style> img{height: 200px !important; width: 100% !important; margin-bottom: 5px;}</style></head><body><p align=\"justify\">"+(aboutUsInfo?.contactInfo!)!+"</p></body></html>"
                    
                    webView.loadHTMLString(contentToLoad, baseURL: nil)

                }
            }
        }
        Loader.sharedInstance.removeLoader()
    }
    
    func failedToReceiveAboutUs(result: AnyObject, status: Bool) {
        Loader.sharedInstance.removeLoader()
    }
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
}
