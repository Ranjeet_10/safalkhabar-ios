//
//  AboutUsResponse.swift
//  safal
//
//  Created by Bidhee iMac on 9/28/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit
import ObjectMapper

class AboutUsResponse: Mappable {
    var status: String?
    var contactInfo: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status      <- map["status"]
        contactInfo <- map["contactInfo"]
    }
}
