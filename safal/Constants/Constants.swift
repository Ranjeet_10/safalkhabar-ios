//
//  Constants.swift
//  buddhatravels
//
//  Created by Bidhee iMac on 8/9/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit

// Storyboards
let HOME_STORY_BOARD: UIStoryboard = UIStoryboard(name: "Dashboard", bundle: nil)
let SLIDER_STORY_BOARD: UIStoryboard = UIStoryboard(name: "Slider", bundle: nil)
let VEHICLE_LIST_BOARD: UIStoryboard = UIStoryboard(name: "VehicleList", bundle: nil)
let SERVICE_DATE_STORY_BOARD: UIStoryboard = UIStoryboard(name: "ServicingDate", bundle: nil)
let SERVICE_REMINDER_STORY_BOARD: UIStoryboard = UIStoryboard(name: "ServicingReminder", bundle: nil)
let VEHICLE_DETAILS_STORY_BOARD: UIStoryboard = UIStoryboard(name: "VehicleDetails", bundle: nil)
let SPARE_PARTS_STORY_BOARD: UIStoryboard = UIStoryboard(name: "SpareParts", bundle: nil)
let SHOWROOM_STORY_BOARD: UIStoryboard = UIStoryboard(name: "ShowRoomLocater", bundle: nil)
let SERVICE_CENTER_STORY_BOARD: UIStoryboard = UIStoryboard(name: "ServiceCenter", bundle: nil)
let INQUIRY_STORY_BOARD: UIStoryboard = UIStoryboard(name: "Inquiry", bundle: nil)
let CONTACTUS_STORY_BOARD: UIStoryboard = UIStoryboard(name: "ContactUs", bundle: nil)
let NOTICE_STORY_BOARD: UIStoryboard = UIStoryboard(name: "Notice", bundle: nil)
let MODEL_WATCH_STORY_BOARD : UIStoryboard = UIStoryboard(name: "ModelWatch", bundle: nil)
let VIDEOS_STORY_BOARD: UIStoryboard = UIStoryboard(name: "Videos", bundle: nil)
let ABOUT_US_STORY_BOARD: UIStoryboard = UIStoryboard(name: "AboutUs", bundle: nil)
let SETTINGS_STORY_BOARD: UIStoryboard = UIStoryboard(name: "Settings", bundle: nil)

//API
let BASE_URI = "https://www.safalkhabar.com"
let MENU_API = BASE_URI + "/api/v1/category/list"
let BREAKING_NEWS_API = BASE_URI + "/api/v1/breaking-news"
let LATEST_NEWS_API = BASE_URI + "/api/v1/latest-news"
let IMPORTANT_NEWS_API = BASE_URI + "/api/v1/important-news"
let TRENDING_NEWS_API = BASE_URI + "/api/v1/trending-news"
let CATEGORY_NEWS_API = BASE_URI + "/api/v1/news"
let FEATURE_PHOTO_API = BASE_URI + "/api/v1/photo-feature"
let MODEL_WATCH_API = BASE_URI + "/api/v1/model-watch"
let DETAILS_API = BASE_URI + "/api/v1/news/"
let PHOTO_FEATURE_API = BASE_URI + "/api/v1/photo-feature"
let VIDEOS_LIST_API = BASE_URI + "/api/v1/videos"
let REGISTER_DEVICE_API = BASE_URI + "/api/v1/register-device"
let ABOUT_US_API = BASE_URI + "/api/v1/contact-information"
let IMAGE_SLIDER_API = BASE_URI + "/api/v1/model-watch/"

let hasBeenLaunchedBeforeFlag = "hasBeenLaunchedBeforeFlag"
let checkNotificationsStatus = "checkNotificationsStatus"


//colors
let APP_PRIMARY_COLOR = Utility.sharedInstance.hexStringToUIColor(hex: "DF3237")
