//
//  AppDelegate.swift
//  agni
//
//  Created by Bidhee iMac on 8/16/18.
//  Copyright © 2018 bidhee pvt. All rights reserved.
//

import UIKit
import CoreData
import MXParallaxHeader
import OneSignal
import Alamofire


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, DashboardAPIDelegate {

    var window: UIWindow?
    fileprivate func createMenuView(){
        
        //let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        
        let mainViewController = HOME_STORY_BOARD.instantiateViewController(withIdentifier: "MXScrollingViewController") as! MXScrollingViewController
        
        let leftMenuViewController = SLIDER_STORY_BOARD.instantiateViewController(withIdentifier: "SliderVc") as! SliderVc
//        let rightMenuViewController = storyboard.instantiateViewController(withIdentifier: "NavigationVC") as! NavigationVC
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        //let redImage = UIImage(named: "")

        nvc.navigationBar.setBackgroundImage(UIImage(named: "primary"), for: .default)
        //nvc.navigationBar.shadowImage = UIImage()
        nvc.navigationBar.isHidden = false
        nvc.navigationBar.backgroundColor = Utility.sharedInstance.hexStringToUIColor(hex: "DF3237")
        nvc.navigationBar.tintAdjustmentMode = .normal
        
        
        
        let slideMenuController = SlideMenuController(mainViewController:nvc, leftMenuViewController: leftMenuViewController)
        //      slideMenuController.automaticallyAdjustsScrollViewInsets = true
        //slideMenuController.delegate = mainViewController as? SlideMenuControllerDelegate
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
        
    }



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions:
        
        [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.createMenuView()
        UINavigationBar.appearance().barTintColor = Utility.sharedInstance.hexStringToUIColor(hex: "DF3237")
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        
        UINavigationBar.appearance().tintColor = UIColor.white
        
        //register device
        let deviceId = UIDevice.current.identifierForVendor!.uuidString
        let versionCode = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        
        if UserDefaults.standard.isFirstLaunch(){
            let registerAPI = DashboardAPI()
            registerAPI.delegate = self
            registerAPI.registerDevice(deviceId: deviceId, appVersion: versionCode)
            
            //set the default notification status to true
            UserDefaults.standard.set(true, forKey: checkNotificationsStatus)
        }
        
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: true]
        
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            
            //This is called when user clicks on notification
            print("UserNotification","received")
            let payload: OSNotificationPayload = result!.notification.payload
            
            
            if payload.additionalData != nil {
                let additionalData = payload.additionalData
                print("Push Add Data = \(additionalData!)")
                let newsId : Int = additionalData!["id"]! as! Int
                
//                if state.rawValue == 0 {
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: NOTIFICATION_RECEIVED_KEY), object: nil, userInfo: ["type":type,"typeId":typeId])
//
//                }
              //  else {
                    if let controller = SERVICE_CENTER_STORY_BOARD.instantiateViewController(withIdentifier: "ServiceCenterVC") as? ServiceCenterVC {
                        controller.newsId = newsId
                        
                        if let window = self.window, let rootViewController = window.rootViewController {
                            var currentController = rootViewController
                            while let presentedController = currentController.presentedViewController {
                                currentController = presentedController
                            }
                        
                            //currentController.present(controller, animated: true, completion: nil)
                            
                            ((currentController as! SlideMenuController).mainViewController as! UINavigationController).pushViewController(controller, animated: false)
                        }
                    }
                    
              //  }
                
                
                
            }
        }
        
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            
            //NotificationCenter.default.post(name: Notification.Name(rawValue: PUSH_NOTIFICATION_RECEIVED_KEY), object: self)
            
            print("Received Notification: \(notification!.payload.notificationID)")
            print("launchURL = \(notification?.payload.launchURL ?? "None")")
            print("content_available = \(notification?.payload.contentAvailable ?? false)")
        }
        
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        
        
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "1a3b5b77-1529-4aca-bc65-0f00ea1107ba", handleNotificationReceived: notificationReceivedBlock,
                                        handleNotificationAction: notificationOpenedBlock,
                                        settings: onesignalInitSettings)
        
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        
//        OneSignal.promptForPushNotifications(userResponse: { accepted in
//            print("User accepted notifications: \(accepted)")
//            print("UserNotification","received2")
//            let state = UIApplication.shared.applicationState
//
//            if state == .background {
//                // background
//            }
//            else if state == .active {
//                print("User Accepted","Notification")
//                NotificationCenter.default.post(name: Notification.Name(rawValue: PUSH_NOTIFICATION_RECEIVED_KEY), object: self)
//            }
//        })

        
        //OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        
        
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    func didRegisterDeviceSuccessfully(resultDict: AnyObject, resultStatus: Bool) {
        print(resultDict)
        
        if(resultDict["status"] as! String == "success") {
            UserDefaults.standard.set(true, forKey: hasBeenLaunchedBeforeFlag)
            UserDefaults.standard.synchronize()
        }
    }
    
    func didFailedDeviceRegistration(_ error: NSError, resultStatus: Bool) {
        print(error.debugDescription)
    }
    
    // MARK: - Core Data stack
    
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "safal")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if #available(iOS 10.0, *) {
            let context = persistentContainer.viewContext
            if context.hasChanges {
                do {
                    try context.save()
                } catch {
                    // Replace this implementation with code to handle the error appropriately.
                    // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    let nserror = error as NSError
                    fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                }
            }
        } else {
            // Fallback on earlier versions
        }
        
    }


}

